<?php



class InventoryManagementSystem
{
    private $host="localhost";
    private $user="dpoulos";
    private $pwd="bioshock";
    private $db="MovieStoreSystem";
    private $database;


    public function __construct()
    {
        $this->database = mysqli_connect($this->host,$this->user,$this->pwd,$this->db);
    }

    public function insertMovie($M_Name, $M_Description, $M_Genre, $M_Price, $M_Image)
    {
        $query = "INSERT INTO Movie (Movie_Name, Movie_Description, Movie_Genre, Movie_Price, Movie_Picture)
                  VALUES ('$M_Name','$M_Description','$M_Genre', '$M_Price', '$M_Image');";
        $this->database->query($query);
    }


    public function removeMovie($M_ID)
    {
        $query = "DELETE FROM Movie
                  WHERE Movie_ID = $M_ID";
        $this->database->query($query);
    }


    public function editMovie($M_ID,$M_Name,$M_Description, $M_Genre, $M_Price)
    {
        $query = "UPDATE Movie
                  SET Movie_Name='$M_Name', Movie_Description ='$M_Description',
                      Movie_Genre = '$M_Genre', Movie_Price = '$M_Price'
                  WHERE Movie_ID = $M_ID";
        $this->database->query($query);
    }


    //pre: none
    //post: Associative array containing all movies.
    public function getAllMovies()
    {
        $query = "SELECT * FROM Movie";
        $result = $this->database->query($query);
        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }


    public function getMoviesBasedOnSpecificGenre($genre)
    {
        $query = "SELECT * FROM Movie WHERE Movie_Genre='$genre'";
        $result = $this->database->query($query);
        return mysqli_fetch_all($result,MYSQLI_ASSOC);

    }

    public function getMoviesBasedOnSearch($searchInput)
    {
        if($searchInput!="")
        {
            //Not as fancy, but still works if they type in the whole name.
            $query = "SELECT * FROM Movie WHERE Movie_Name='$searchInput' 
                          OR Movie_Genre='$searchInput' 
                          OR Movie_Id='$searchInput'";

            $query2 = "SELECT * FROM Movie WHERE Movie_Name LIKE '%". addslashes($searchInput) ."%' 
            OR Movie_Genre LIKE '%". addslashes($searchInput) ."%'
            OR Movie_ID LIKE '%". addslashes($searchInput) ."%'";

            $result = $this->database->query($query2);
            return mysqli_fetch_all($result,MYSQLI_ASSOC);

        }

    }


    public function displayAllMoviesBasedOnSearchInput($accType,$movieTitle)
    {
        //print '<div class="row">';
        $row = $this->getMoviesBasedOnSearch($movieTitle);
        if(count($row) > 0)
        {
            for($i = 0; $i < count($row); $i++)
            {
                $mid = $row[$i]['Movie_ID'];
                $mname = $row[$i]['Movie_Name'];
                $mdesc = $row[$i]['Movie_Description'];
                $mgenre = $row[$i]['Movie_Genre'];
                $mprice = $row[$i]['Movie_Price'];
                $mpicture = $row[$i]['Movie_Picture'];
                $item = new Movie($mid,$mname,$mdesc,$mgenre,$mprice, $mpicture);

                //print '   <div class="col s3">';
                if($accType == "Admin")
                {
                    $item->displayMovieForAdmin();
                }
                else
                {
                    $item->displayMovieForCustomer();
                }
                //print '</div>';
            }
        }
        //print '</div>';
    }



    public function displayAllMovies($accType)
    {
        //print '<div class="row">';
        $row = $this->getAllMovies();
        if(count($row) > 0)
        {
            for($i = 0; $i < count($row); $i++)
            {
                $mid = $row[$i]['Movie_ID'];
                $mname = $row[$i]['Movie_Name'];
                $mdesc = $row[$i]['Movie_Description'];
                $mgenre = $row[$i]['Movie_Genre'];
                $mprice = $row[$i]['Movie_Price'];
                $mpicture = $row[$i]['Movie_Picture'];
                $item = new Movie($mid,$mname,$mdesc,$mgenre,$mprice, $mpicture);

                //print '   <div class="col s4">';
                if($accType == "Admin")
                {
                    $item->displayMovieForAdmin();
                }
                else
                {
                    $item->displayMovieForCustomer();
                }


                //print '</div>';

            }

        }
        else
        {
            //print "No movies in the system.";
        }
       //print '</div>';
    }


    public function addRatingToMovie($movieId, $rating, $description,$userId)
    {
        $query = "INSERT INTO MovieRating VALUES ('NULL','$movieId','$rating','$description','$userId')";
        $this->database->query($query);
    }

    public function getMovieRatings($movieID)
    {
        $query = "SELECT * FROM MovieRating WHERE Movie_ID='$movieID'";
        $result = $this->database->query($query);
        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }



}
