<?php

/**
 * Created by PhpStorm.
 * User: dpoulos
 * Date: 3/18/16
 * Time: 12:54 PM
 */

require_once ("load.php");


class LoginManager
{
    
    static $_SESSION;

    public function __construct()
    {
        session_start();
    }

    public function processLoginInformation()
    {
        if(isset($_POST['login_Submit']))
        {
            $email = $_POST['login_Email'];
            $password = $_POST['login_Password'];

            if($email == "" or $password =="")
            {
                print ("Form not valid");
            }
            else
            {
                //check if user exists in database
                $user = new UserManagementSystem();
                if($user->isUserInDatabase($email,$password))
                {
                    $details = $user->getUserInformation($email,$password);
                    $accType = $user->convertTypeIDToString($details[0]['User_Type']);
                    $sessionUser = new User($details[0]['User_ID'],
                                           $details[0]['User_First_Name'],
                                           $details[0]['User_Last_Name'],
                                           $details[0]['User_Email'],
                                           $details[0]['User_Password'],
                                           $details[0]['User_Wishlist'],
                                           $accType);
                    $_SESSION['User'] = $sessionUser;

                    header('Location: ./index.php');
                }
                else
                {
                    print("Invalid Email or Password");
                }
            }

        }

    }

    public function processCreateAccountInformation()
    {
        if(isset($_POST['create_Submit']))
        {
            $fName = $_POST['create_First_Name'];
            $lName = $_POST['create_Last_Name'];
            $email = $_POST['create_Email'];
            $pass1 = $_POST['create_Password'];
            $pass2 = $_POST['create_Password2'];

            if($pass1 != $pass2 or $fName =="" or $lName =="" or $email=="" or $pass1 =="" or $pass2 == "")
            {
                print "<h2> Form not completed correctly </h2>";
            }
            else
            {
                $user = new UserManagementSystem();
                if(!$user->isUserInDatabase($email,$pass1))
                {
                    $userM = new UserManagementSystem();
                    $userM ->addUser($fName,$lName,$email,$pass1,NULL,2);
                    print "Account succesfully created";

                }
                else
                {
                   print "Email is already in use.";
                }

            }
        }
    }
    
    public function wipeSession()
    {
        unset($_SESSION);
    }

    public function destroySession()
    {
        session_destroy();
    }

    public function checkIfUserExists()
    {
        return true;
    }

    public function __toString()
    {
        return print_r($_SESSION,TRUE);
    }

    public function logout()
    {
        $this->destroySession();
        header('Location: ./login.php');
    }

}