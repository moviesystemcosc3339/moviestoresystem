<?php

/**
 * Created by PhpStorm.
 * User: dpoulos
 * Date: 2/28/16
 * Time: 11:47 PM
 */
class Movie
{
    private $Movie_ID;
    private $Movie_Name;
    private $Movie_Description;
    private $Movie_Genre;
    private $Movie_Price;
    private $Movie_Picture;


    public function __construct($M_ID, $M_Name, $M_Description, $M_Genre, $M_Price,$M_Picture="")
    {
        $this->Movie_ID = $M_ID;
        $this->Movie_Name = $M_Name;
        $this->Movie_Description = $M_Description;
        $this->Movie_Genre = $M_Genre;
        $this->Movie_Price = $M_Price;
        $this->Movie_Picture = $M_Picture;

    }


    public function getMovieID()
    {
        return $this->Movie_ID;
    }

    public function getMovieName()
    {
        return $this->Movie_Name;
    }

    public function getMovieDescription()
    {
        return $this->Movie_Description;
    }

    public function getMovieGenre()
    {
        return $this->Movie_Genre;
    }

    public function getMoviePrice()
    {
        return $this->Movie_Price;
    }

    public function getMoviePicture()
    {
        return $this->Movie_Picture;
    }

    public function displayMovieForAdmin()
    {

        print
            '     <div class="card-panel black">'.
            '        <div class="card-content white-text" style="font-family: Serif; font-size: 20px;">'.
            '             <div class="right align"><img src="'.$this->getMoviePicture().'" height="125" width="125" class="right-align"/></div>'.
            '             <span class="card-title">Movie Title: '.$this->getMovieName().'</span>'.
            '             <li>Movie ID: '.$this->getMovieID().'</li>'.
            '             <li>Description: '.$this->getMovieDescription().'</li>'.
            '             <li>Genre: '.$this->getMovieGenre().'</li>'.
            '             <li>Price: $'.$this->getMoviePrice().'</li>';
        print '        </div>'.
        '         <div class="card-action">';
        print "        <form method='post'>\n".
            "             <input type='submit' name='delete' value='Delete' class='btn deep-purple darken-2'>".
            "             <input type='hidden' name='Movie_ID_delete' value='".$this->getMovieID()."'>".
            "         </form>".
            "</a>";

        print '        </div>'.
            '   </div>';



    }


    public function displayMovieForCustomer()
    {
        print
            '     <div class="card-panel black left-align">'.
            '        <div class="card-content white-text" style="font-family: Serif; font-size: 20px;">'.
            '             <div class="right align"><img src="'.$this->getMoviePicture().'" height="125" width="125" class="right-align"/></div>'.
            '             <span class="card-title">Movie Title: '.$this->getMovieName().'</span>'.
            '             <li>Movie ID: '.$this->getMovieID().'</li>'.
            '             <li>Description: '.$this->getMovieDescription().'</li>'.
            '             <li>Genre: '.$this->getMovieGenre().'</li>'.
            '             <li>Price: $'.$this->getMoviePrice().'</li>';
        print '        </div>'.
            '         <div class="card-action">';
        print "        <form method='post'>\n".
            "             <input type='hidden' name='Movie_ID' value='".$this->getMovieID()."'>".
            "             <input type='submit' name='Add_Movie_To_Cart' value='Add' class='btn deep-purple darken-2'>".
            "             <input type='submit' name='Add_To_Wishlist' value='Wishlist' class='btn deep-purple darken-2'>".

            "    <ul class='collapsible' data-collapsible='accordion'>".
            "        <li>".
            "            <div class='collapsible-header'><i class='material-icons'>grade</i>Rate Movie</div>".
            "              <div class='collapsible-body'><p>".
            "                <div class='container input-field'>".
            "                   <h6 style='color:crimson;'> Enter Rating </h2>".
            "                    <input type='text' name='Movie_Rating_Description' placeholder='Description' style='color:white'>".
            "                    <input type='text' name='Movie_Rating' placeholder='1/5' style='color:white'>".
            "                    <input type='submit' name='Rate_Movie' value='Rate' class='btn deep-purple darken-2' style='color:white'>".
            "               </div>".
            "              </p></div>".
            "        </li>".
            "        <li>".
            "            <div class='collapsible-header'><i class='material-icons'>view_list</i>View Ratings</div>".
            "              <div class='collapsible-body'><p>".
            "                <div class='container'>".
            "                   <h6 style='color:crimson;'> Previous Ratings </h2>";
            $this->displayPreviousMovieRatings();
            print "           </div>".
            "              </p></div>".
            "        </li>".
            "      </ul>".
            "         </form>".
            "</a>";

        print '        </div>'.
              '   </div>';

    }

    function displayPreviousMovieRatings()
    {
        $inv = new InventoryManagementSystem();
        $ratingsArray = $inv->getMovieRatings($this->getMovieID());
        if(count($ratingsArray) > 0)
        {
            for ($i = 0; $i < count($ratingsArray); $i++)
            {
                print "<br><h6 style='color: green'> Rating: </h6>
                        <h6 style='color:white'>".$ratingsArray[$i]['Movie_Rating']."/5 Stars</h6>
                        <h6 style='color: green'>User Feedback: </h6><h6 style='color:white'>".$ratingsArray[$i]['Movie_Rating_Description']."</h6>
                        <h6 style='color: green'>User: </h6><h6 style='color:white'>".$ratingsArray[$i]['Movie_Rating_User']."</h6>";

            }

        }
        else
        {
            print "<h5 style='color:crimson'>Currently no ratings.</h5>";
        }
    }



    function __toString()
    {
        return 'ID:          '.$this->Movie_ID.' '.
               'NAME:        '.$this->Movie_Name.' '.
               'DESCRIPTION: '.$this->Movie_Description.' '.
               'GENRE:       '.$this->Movie_Genre.' '.
               'PRICE:       '.$this->Movie_Price.' ';
    }
}







