<?php


require_once ("load.php");

class Search
{


    public function SearchControl()
    {
        $inv = new InventoryManagementSystem();

        if(empty($_POST))
        {
            //Do nothing.
        }
        else if(isset($_POST['submit']))
        {
            $this->handleAddMovie();
        }
        else if(isset($_POST['delete']))
        {
            $this->handleDelete();

        }
        else if(isset($_POST['Rate_Movie']))
        {
            $this->handleRating();
        }
        else if(isset($_POST['Logout']))
        {
            $this->handleLogout();
        }

    }



    public function handleRating()
    {
        $inv = new InventoryManagementSystem();
        $movieId = $_POST['Movie_ID'];
        $description = $_POST['Movie_Rating_Description'];
        $rating = $_POST['Movie_Rating'];

        //Default User Account.
        $loggedInUser = new User("", "Guest", "", "", "", "",2);

        //Switched to Logged in User.
        if(isset($_SESSION['User']))
        {
            $loggedInUser = $_SESSION['User'];
        }

        if($description == "" OR $rating > 5 OR $rating < 1)
        {
            print "<h6> Rating form not completed correctly </h6>";
        }
        else
        {
            $inv->addRatingToMovie($movieId,$rating,$description,$loggedInUser->getUserFirstName());
            print "<h4> Successfully added Rating to Movie #".$movieId."</h4>";
        }
    }

    public function handleDelete()
    {
        $inv = new InventoryManagementSystem();
        $idtodelete = $_POST['Movie_ID_delete'];
        $inv->removeMovie($idtodelete);
    }

    public function handleAddMovie()
    {
        $inv = new InventoryManagementSystem();
        $name = $_POST['Movie_Name'];
        $desc = $_POST['Movie_Description'];
        $genre = $_POST['Movie_Genre'];
        $price = $_POST['Movie_Price'];
        $image =  $_POST['Movie_Picture'];

        if($name != "" and $desc != "" and $genre !="" and $price != 0)
        {

            $inv->insertMovie($name, $desc, $genre, $price, $image);
            print "<h4 id='info' class='center-align'> Successfully added.</h4>";
        }
        else
        {
            print "<h4 id='info' class='center-align'> Form not completed correctly.</h4>";
        }
    }

    public function handleLogout()
    {
        $login = new LoginManager();
        $login->logout();
    }
}