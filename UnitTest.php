<?php

require_once ("load.php");

class UnitTest extends PHPUnit_Framework_TestCase
{
    public function testConvertingAccountTypeIds()
    {
        $a = new UserManagementSystem();
        $this->assertEquals("Customer", $a->convertTypeIDToString(2));
    }

    public function testIfUserInDatabase()
    {
        $a = new UserManagementSystem();
        $a->addUser("David","Poulos","david@stedwards.edu", "taco",1,2);
        $this->assertEquals(true, $a->isUserInDatabase("david@stedwards.edu","taco"));
    }

    public function testIfConvertAccountTypeIdFails()
    {
        $a = new UserManagementSystem();
        $this->assertEquals("Admin", $a->convertTypeIDToString(3));
    }


//    public function testFailure()
//    {
//        $this->assertFalse(true);
//    }

}


