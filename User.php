<?php

/**
 * Created by PhpStorm.
 * User: dpoulos
 * Date: 3/24/16
 * Time: 9:23 PM
 */
class User
{
    private $User_ID;
    private $User_First_Name;
    private $User_Last_Name;
    private $User_Email;
    private $User_Password;
    private $User_Wish_List;
    private $User_Type;
    
    public function __construct($id,$first,$last,$email,$pass,$wish,$type)
    {
        $this->User_ID = $id;
        $this->User_First_Name = $first;
        $this->User_Last_Name = $last;
        $this->User_Email = $email;
        $this->User_Password = $pass;
        $this->User_Wish_List = $wish;
        $this->User_Type = $type;
    }



    public function getUserID()
    {
        return $this->User_ID;
    }

    public function getUserFirstName()
    {
        return $this->User_First_Name;
    }

    public function getUserLastName()
    {
        return $this->User_Last_Name;
    }

    public function getUserEmail()
    {
        return $this->User_Email;
    }

    public function getUserPassword()
    {
        return $this->User_Password;
    }

    public function getUserWishList()
    {
        return $this->User_Wish_List;
    }

    public function getUserType()
    {
        return $this->User_Type;
    }

    public function __toString()
    {
        return $this->User_First_Name." ".$this->getUserLastName();
    }



}