<?php

require_once "load.php";

//Dependencies.
$login = new LoginManager();
$inv = new InventoryManagementSystem();

//Makes decisions based on POST data.
$search = new Search();
$search->SearchControl();

//Default User Account.
$loggedInUser = new User("", "Guest", "", "", "", "","Guest");

//Change Logged in user from default.
if(isset($_SESSION['User']))
{
    $loggedInUser = $_SESSION['User'];
}
?>
<!DOCTYPE html>
<head>
    <title>Browse Selection</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link rel="stylesheet" href="css/indexcss.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>

<?php if($loggedInUser->getUserType() == "Guest")
{
    print "<div class='container right-align'>";
    print "<a href='./login.php' class='btn'>Login</a>";
    print "</div>";
}
else
{
    print "<div class='container right-align'>";
    print "<form action='".$_SERVER['PHP_SELF']."' method='post'>";
    print "<h5>Welcome ".$loggedInUser."</h5>";
    print "<input type='submit' class='btn' name='Logout' value='Logout'>";
    print "</form>";
    print "</div>";
}
?>

<div class="container">
<h1 class="center" style="color: black;font-family: Futura;">BlockBusted</h1>

<nav class="deep-purple darken-2">
    <div class="nav-wrapper">
<!--        <form action="--><?php //$_SERVER['PHP_SELF']?><!--" method="post">-->
            <div class="input-field ">
                <input id="search" type="search" placeholder="Movie Title/Genre/ID" required>
                <label for="search"><i class="material-icons">search</i></label>
                <i class="material-icons">close</i>
            </div>
<!--        </form>-->
    </div>
</nav>


<?php if ($loggedInUser->getUserType() == "Admin") { ?>
    <div class="container input-field" id = "color" >
    <form method = "post" action = "<?php $_SERVER['PHP_SELF']?>" enctype = "multipart/form-data" >
        <h5 class="left" id = "info" > Add Movie:</h5 >
        <input type = "text" name = "Movie_Name" placeholder = "Name" >
        <input type = "text" name = "Movie_Description" placeholder = "Description" >
        <input type = "text" name = "Movie_Genre" placeholder = "Genre" >
        <input type = "text" name = "Movie_Price" placeholder = "Price $0" >
        <input type = "text" name = "Movie_Picture" placeholder = "Link to Picture" >
        <input type = "submit" name = "submit" value = "Submit" class="right btn center deep-purple darken-2" >
    </form >
</div >
<?php }?>

<br>
<br>

<div class="container">
        <div id="defaultMovieDisplay"><?php $inv->displayAllMovies($loggedInUser->getUserType()); ?></div>
        <div id ="searchMovieDisplay"></div>
</div>

</div>
</body>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script>
//    $(document).ready(function(){
        $('.modal-trigger').leanModal();
        $('.collapsible').collapsible({
            accordion : false
        });
//    });

</script>
<script src="js/indexjs.js"></script>


</html>
