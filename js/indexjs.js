/**
 * Created by David on 2/28/2016.
 */
$(document).ready(function() {
    // $('select').material_select();
    // $('.carousel').carousel();



    //Begin checking what the user is typing.
    $('#search').keyup(function(){
        var searchText = $('#search').val();

        //Get the movies out of the database based on the typing.
        $.post("searchResponse.php", {movieTitle:searchText},
            function (response)
            {
                $("#searchMovieDisplay").html(response);
            }).error(function(){
            alert('error');
        });


        //If they current search bar is empty display default.
        console.log(searchText);
        if(!searchText=="")
        {
            $('#defaultMovieDisplay').hide();
        }
        else
        {
            $('#defaultMovieDisplay').show();
        }
    });
});

