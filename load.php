<?php
/**
 * Created by PhpStorm.
 * User: dpoulos
 * Date: 3/28/16
 * Time: 2:58 PM
 */

require_once(dirname(__FILE__) . '/InventoryManagementSystem.php');
require_once(dirname(__FILE__) . '/LoginManager.php');
require_once(dirname(__FILE__) . '/Movie.php');
require_once(dirname(__FILE__) . '/Search.php');
require_once(dirname(__FILE__) . '/User.php');
require_once(dirname(__FILE__) . '/UserManagementSystem.php');
?>