<?php
require_once ("load.php");
$login = new LoginManager();
if(isset($_SESSION['User']))
{
    header('Location: ./index.php');
}
else
{
    $login->processLoginInformation();
    $login->processCreateAccountInformation();
}


?>

<html>
<head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link rel="stylesheet" href="css/login.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
</head>
<body class="container">






<div class="container>">
    <div class="input-field">
        <h3 style="color:#512da8;">Login</h3>
        <form method="post" action="<?php $_SERVER['PHP_SELF']?>">
            <input type="text" name="login_Email" placeholder="Email">
            <input type="password" name="login_Password" placeholder="Password">
            <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Create Account</a>
            <a href="index.php" class="btn">Home</a>
            <input type="submit" name="login_Submit" value="Login" class="btn deep-purple darken-2">
        </form>
    </div>
<!-- Modal Trigger -->

</div>
<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="container input-field">
        <h3 style="color:#512da8;">Create Account</h3>
        <form method="post" action="<?php $_SERVER['PHP_SELF']?>">
            <input type="email" name="create_Email" placeholder="Email">
            <input type="text" name="create_First_Name" placeholder="First Name">
            <input type="text" name="create_Last_Name" placeholder="Last Name">
            <input type="password" name="create_Password" placeholder="Password">
            <input type="password" name="create_Password2" placeholder="Re-Type Password">
            <input type="submit" name="create_Submit" value="Create Account" class="btn deep-purple darken-2">
        </form>
    </div>
</div>




</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script>
    $(document).ready(function(){
        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        $('.modal-trigger').leanModal();
    });

</script>


</html>