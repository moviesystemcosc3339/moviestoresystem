<?php
/**
 * Created by PhpStorm.
 * User: dpoulos
 * Date: 4/7/16
 * Time: 11:51 AM
 */

require_once ('load.php');
//Dependencies
$user = new LoginManager();
$inv = new InventoryManagementSystem();

//Default User Account.
$loggedInUser = new User("", "", "", "", "", "",2);

//Switched to Logged in User.
if(isset($_SESSION['User']))
{
    $loggedInUser = $_SESSION['User'];
}

$inv->displayAllMoviesBasedOnSearchInput($loggedInUser->getUserType(),$_POST['movieTitle']);

?>

<!--Make sure the Movies still function correctly -->
<script>

    $('.modal-trigger').leanModal();
    $('.collapsible').collapsible({
        accordion : false
    });
</script>

